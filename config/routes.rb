Rails.application.routes.draw do
  namespace :api  do
  	namespace :v1 do
  	  # post 'auth/register', to: 'users#register'
  	  post 'auth/login', to: 'users#login'
  	  post 'auth/register', to: 'users#register'
  	  get '/max' , to: 'users#max'
  	  resources :events
  	end
  end
  # post 'auth/login', to: 'users#login'
  # post 'auth/register', to: 'users#register'
  # resources :events
end
